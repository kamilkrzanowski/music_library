from .models import Album 
from rest_framework import viewsets, permissions
from .serializers import AlbumSerializer 

# Artists Viewset

class AlbumsViewset(viewsets.ModelViewSet):
  permission_classes = [
    permissions.AllowAny
  ]
  serializer_class = AlbumSerializer

  def get_queryset(self):
    artist_id = self.request.query_params.get('artist_id')
    
    if(artist_id):
      queryset = Album.objects.filter(artist_id = artist_id)
    else:
      queryset = Album.objects.all()
    return queryset
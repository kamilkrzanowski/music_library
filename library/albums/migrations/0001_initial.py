# Generated by Django 2.1.7 on 2019-03-13 19:24

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Album',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('artist_id', models.IntegerField(default=0)),
                ('name', models.CharField(max_length=100)),
                ('year', models.IntegerField(default=0)),
                ('songs', models.IntegerField(default=0)),
            ],
        ),
    ]

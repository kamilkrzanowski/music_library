from django.db import models

# Create your models here.

class Album(models.Model):
  artist_id = models.IntegerField(default = 0)
  name = models.CharField(max_length = 100)
  year = models.IntegerField(default = 0)
  songs = models.IntegerField(default = 0)
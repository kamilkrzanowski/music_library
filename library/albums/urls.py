from rest_framework import routers
from .api import AlbumsViewset

router = routers.DefaultRouter()
router.register('api/albums', AlbumsViewset, 'albums')
router.register('api/albums/?artist_id=<int:id>', AlbumsViewset , base_name='albums')
urlpatterns = router.urls
from django.db import models

# Create your models here.

class Artist(models.Model):
  name = models.CharField(max_length = 100)
  genre = models.CharField(max_length = 100)
  country = models.CharField(max_length = 100)
  image = models.CharField(max_length = 200, blank = True)
  body = models.CharField(max_length = 500, blank=True)
  created_at = models.DateTimeField(auto_now_add=True)
from rest_framework import routers
from .api import ArtistsViewset

router = routers.DefaultRouter()
router.register('api/artists', ArtistsViewset, 'artists')

urlpatterns = router.urls
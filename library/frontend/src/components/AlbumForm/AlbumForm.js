import React, { Component } from 'react'
import axios from 'axios'
import { Transition } from 'react-transition-group'
import PropTypes from 'prop-types'

class AlbumForm extends Component {
  constructor(props) {
    super(props)
    this.state = {
      albumForm: {
        name: '',
        songs: '',
        year: ''
      },
      InvalidFields: {
        name: false,
        songs: false,
        year: false
      },
      albumFormError: false
    }
  } 

  addNewAlbumHandler = e => {
    e.preventDefault()

    const newAlbum = {
      artist_id: this.props.artist_id,
      name: this.state.albumForm.name,
      year: this.state.albumForm.year,
      songs: this.state.albumForm.songs,
    }

    for(let key in newAlbum ) {
      if (newAlbum.hasOwnProperty(key)) {
        this.validateField(key, newAlbum[key])
     }
    }

    axios.post('http://localhost:8000/api/albums/', newAlbum)
      .then(response => {
        this.setState({
          albumForm: {
            name: '',
            songs: '',
            year: ''
          }
        })
        this.props.refresh()
      }).catch( error => {
        this.setState({
          albumFormError: true
        })
      })
  }

  removeErrorBox = e => {
    setTimeout(() => {
      this.setState({
        albumFormError: false
      })
    }, 4000);
  }

  onInputChangeHandler = e => {
    const fieldName = e.target.name
    const fieldValue = e.target.value

    this.setState( prevState => ({
      albumForm: {
        ...prevState.albumForm,
        [fieldName] : fieldValue
      }
    }))

    this.validateField(fieldName, fieldValue)
  }

  validateField(fieldName, fieldValue) {
    this.setState( prevState => ({
      InvalidFields: {
        ...prevState.InvalidFields,
        [fieldName]: fieldValue.length < 1
      }
    }))
  }

  render() {
    return (
    <form className="AlbumForm">
      <h2 className="AlbumForm__heading t-heading">Add new album</h2>
      <div className="AlbumForm__labelWrapper Form__row">
        <label className="AlbumForm__label Form__label t-paragraph" htmlFor="name"> Album name* </label>
        <input className={ "AlbumForm__input Form__input" + ( this.state.InvalidFields.name ? ' is-invalid' : '' ) } type="text" id="name" name="name" value={ this.state.albumForm.name } onChange={ this.onInputChangeHandler }/>
      </div>
      <div className="AlbumForm__labelWrapper Form__row">
        <label className="AlbumForm__label Form__label t-paragraph" htmlFor="songs">Number of songs* </label>
        <input className={ "AlbumForm__input Form__input" + ( this.state.InvalidFields.songs ? ' is-invalid' : '' ) } min="1" type="number" id="songs" name="songs" value={ this.state.albumForm.songs } onChange={ this.onInputChangeHandler }/>
      </div>
      <div className="AlbumForm__labelWrapper Form__row">
        <label className="AlbumForm__label Form__label t-paragraph" htmlFor="year"> Release year* </label>
        <input className={ "AlbumForm__input Form__input" + ( this.state.InvalidFields.year ? ' is-invalid' : '' ) } min="1800" type="number" id="year" name="year" value={ this.state.albumForm.year } onChange={ this.onInputChangeHandler }/>
      </div>
      <button className="AlbumForm__submit btn" onClick={ this.addNewAlbumHandler }>Submit</button>
      <Transition in={ this.state.albumFormError } timeout={ 800 } mountOnEnter unmountOnExit onEntered={ this.removeErrorBox }>
        { state => (
          <p className={"Form__error t-paragraph" + (state === 'entering' ? ' is-entering' : state === 'exiting' ? ' is-exiting' : '') }>Entered values are invalid or some of them are missing. Please check your inputs.</p>
        )}
      </Transition>
    </form>
    )
  }
}

AlbumForm.propTypes = {
  artist_id: PropTypes.string.isRequired,
  refresh: PropTypes.func.isRequired
}

export default AlbumForm

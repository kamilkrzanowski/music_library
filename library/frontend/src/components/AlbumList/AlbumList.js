import React, { Fragment, Component } from 'react'
import PropTypes from 'prop-types'
import axios from 'axios'
import { TransitionGroup, Transition } from 'react-transition-group'

import Modal from '../_ui/Modal/Modal'
import AlbumSvg from '../_ui/Svg/AlbumSvg'
import SongsList from '../SongsList/SongsList'
import AlbumForm from '../AlbumForm/AlbumForm'

class AlbumList extends Component { 
  constructor(props) {
    super(props)
    this.state = {
      albums: [],
      albumForm: {
        album_id: 0,
        album_name: '', 
        album_year: '', 
        album_songs: '',
      },
      InvalidFields: {
        album_name: false,
        album_year: false,
        album_songs: false,
      },
      showEditAlbumModal: false,
      showDeleteAlbumModal: false,
      modal: React.createRef()
    }
  }

  onChangeAlbumDetailsHandler = e => {
    const fieldName = e.target.name
    const fieldValue = e.target.value

    this.setState( prevState => ({
      albumForm: {
        ...prevState.albumForm,
         [fieldName]: fieldValue
      }
    }))
    this.validateField(fieldName, fieldValue)
  }

  validateField(fieldName, fieldValue) {
    this.setState( prevState => ({
      InvalidFields: {
        ...prevState.InvalidFields,
        [fieldName]: fieldValue.length < 1
      }
    }))
  }

  updateExistingAlbumHandler = e => {
    e.preventDefault()

    const updatedAlbum = {
      name: this.state.albumForm.album_name,
      songs: this.state.albumForm.album_songs,
      year: this.state.albumForm.album_year,
    }


    axios.put('http://localhost:8000/api/albums/' + this.state.albumForm.album_id + '/', updatedAlbum)
    .then( response => {
      this.setState( prevState => ({
        albums: [ ...prevState.albums.filter( album => 
          album.id !== response.data.id ),
          response.data
         ]
      }))
      this.onToggleModalHandler(1)
    })
    .catch( error => {

    })
  }

  deleteExistingAlbum = e => {
    e.preventDefault()

    axios.delete('http://localhost:8000/api/albums/' + this.state.albumForm.album_id + '/')
    .then( response => {
      this.setState( prevState => ({
        albums: [ 
          ...prevState.albums.filter( album => 
          album.id !== this.state.albumForm.album_id )
         ]
      }))
    })
    this.onToggleModalHandler(2)
  }

  onToggleTab = e => {
    const clickedTab = e.currentTarget.parentNode;

    if(clickedTab.classList.contains('is-active')) {
      if(closingMenuTimer) clearTimeout(closingMenuTimer) 
      clickedTab.classList.add('is-closing')
      clickedTab.classList.remove('is-active')
 
      const closingMenuTimer = setTimeout(() => {
        clickedTab.classList.remove('is-closing')
       }, 350)
       
     } else {
       clickedTab.classList.add('is-active')
     }
  }

  getArtistsAlbums = () => {
    axios.get('http://localhost:8000/api/albums/?artist_id=' + this.props.artist_id )
    .then(response => {
      this.setState({ albums: response.data })
    })
  }

  componentDidMount() {
    if( this.props.artist_id ) {
      this.getArtistsAlbums()
    }
  }

  componentDidUpdate(prevProps) {
    if(this.props.artist_id !== prevProps.artist_id) {
      this.getArtistsAlbums()
    }
  }

  onToggleModalHandler = (id, albumId = 0) => {
    window.scrollTo(0, this.modal)

    if(albumId > 0) {
      axios.get('http://localhost:8000/api/albums/' + albumId )
      .then(response => {
        this.setState({
          albumForm: {
            album_id: albumId,
            album_name: response.data.name,
            album_year: response.data.year,
            album_songs: response.data.songs
          }
        })
      })
    }

    if (id === 1) {
      this.setState( prevState => ({ 
        showEditAlbumModal: !prevState.showEditAlbumModal 
      }))
    } 
    else if (id === 2){
      this.setState( prevState => ({
         showDeleteAlbumModal: !prevState.showDeleteAlbumModal 
      }))
    }
  }

  render() {
    return (
    <Fragment>
      <TransitionGroup className="AlbumList__albums" component="ul">
        { this.state.albums.map( album => (
          <Transition timeout= { 400 } key={ album.id }>
            { state => ( 
            <li className={ "AlbumList__item" + (state === 'entering' ? ' is-entering' : state === 'exiting' ? ' is-exiting' : '') }> 
              <div className="AlbumList__itemTab" onClick={ this.onToggleTab }>
                <AlbumSvg classes={ "AlbumList__icon" }/>
                <div className="AlbumList__itemContent">
                  <p className="AlbumList__heading t-heading t-heading--small">{ album.name }</p>
                  <div className="AlbumList__itemContentMeta">
                    <p className="AlbumList__releaseYear t-paragraph">Release year: { album.year }</p>
                    <p className="AlbumList__songs t-paragraph">Number of songs: { album.songs }</p>
                  </div>
                </div>
              </div>
              <SongsList album_id={ album.id } toggleModal={ this.onToggleModalHandler } newSong={ this.state.showNewSongModal }/>
            </li>
            )}
          </Transition>
          ))
        }
      </TransitionGroup>
      <AlbumForm artist_id={ this.props.artist_id } refresh={ this.getArtistsAlbums }/>
      <Modal 
        ref={ this.modal }
        title="Edit Album details" 
        inProp={ this.state.showEditAlbumModal } 
        submitTrigger={ this.updateExistingAlbumHandler } 
        closeTrigger={ this.onToggleModalHandler.bind(this, 1) }
      >
        <div className="Form__row">
          <label className="Form__label t-paragraph" htmlFor="album_name">Album name*</label>
          <input className={ "Form__input" + ( this.state.InvalidFields.album_name ? ' is-invalid' : '' ) } name="album_name" id="album_name" type="text" onChange={ this.onChangeAlbumDetailsHandler } value={ this.state.albumForm.album_name }/>
        </div>
        <div className="Form__row">
          <label className="Form__label t-paragraph" htmlFor="album_name">Release year*</label>
          <input className={  "Form__input" + ( this.state.InvalidFields.album_year ? ' is-invalid' : '' ) } name="album_year" id="album_year" type="number" onChange={ this.onChangeAlbumDetailsHandler } value={ this.state.albumForm.album_year }/>
        </div>
        <div className="Form__row">
          <label className="Form__label t-paragraph" htmlFor="album_name"># of songs*</label>
          <input className={ "Form__input" + ( this.state.InvalidFields.album_songs ? ' is-invalid' : '' ) } name="album_songs"  id="album_songs" type="number" onChange={ this.onChangeAlbumDetailsHandler } value={ this.state.albumForm.album_songs }/>
        </div>
        <Transition in={ this.state.InvalidFields.album_year || this.state.InvalidFields.album_songs || this.state.InvalidFields.album_name } timeout={ 400 } mountOnEnter unmountOnExit>
        { state => (
          <div className="Form__row">
            <p className={"Form__error" + (state === 'entering' ? ' is-entering' : state === 'exiting' ? ' is-exiting' : '') }>All fields are required!</p>
          </div>
        )}
        </Transition>
      </Modal> 
      <Modal 
        ref={ this.modal }
        title="Delete selected album?" 
        inProp={ this.state.showDeleteAlbumModal } 
        submitTrigger={ this.deleteExistingAlbum } 
        closeTrigger={ this.onToggleModalHandler.bind(this, 2) } 
        cancel={ true }
      />
    </Fragment>
    )
  }
}

AlbumList.propTypes = {
  artist_id: PropTypes.string.isRequired
}

export default AlbumList

import 'react-app-polyfill/ie11'
import React, { Component, Fragment } from 'react'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import axios from 'axios'

import HeaderComponent from '../Header/Header'
import FooterComponent from '../Footer/Footer'
import ContentWrapper from '../_ui/ContentWrapper/ContentWrapper'
import ItemsListing from '../ItemsListing/ItemsListing'
import SingleArtist from '../SingleArtist/SingleArtist'
import ArtistForm from '../ArtistForm/ArtistForm'

class App extends Component {
  state = {
    artists: [],
    filterResults: [],
    openSideMenu: false
  }

  onStartFilterHandler = e => {
    const searchedValue = e.target.value
    if( searchedValue === ' ' || searchedValue === '' ) 
      document.querySelector('[data-filter-result]').classList.remove('is-active')
    else
      document.querySelector('[data-filter-result]').classList.add('is-active')
    this.setState( prevState => ({
      filterResults: [
        ...prevState.artists.filter( artist => (
           artist.name.toLowerCase().includes( searchedValue.toLowerCase().trim() )
        ))
      ]
    }))
  }

  toggleMobileMenuHandler = e => {
    document.querySelector('[data-menu]').classList.toggle('is-active')

    this.setState( prevState => ({ openSideMenu: !prevState.openSideMenu }))
  }

  componentDidMount = () => {
    axios.get('http://localhost:8000/api/artists')
      .then(response => {
        this.setState({ artists: response.data })
      })
  }

  componentDidUpdate = () => {
    axios.get('http://localhost:8000/api/artists')
      .then(response => {
        this.setState({ artists: response.data })
      })
  }
  
  render() {
    return (
      <Router>
        <Fragment>
          <HeaderComponent toggleMenu={ this.toggleMobileMenuHandler } menuState={ this.state.openSideMenu } changed={ this.onStartFilterHandler } results={ this.state.filterResults }/>
            <ContentWrapper>
              <Switch>
                <Route exact path="/" render={ props => (
                  <ItemsListing classes={ ArtistsClasses } items={ this.state.artists }/>
                )} />
                <Route path="/add-artist" component={ ArtistForm } />
                <Route path="/edit-artist/:id" component={ ArtistForm } />
                <Route path="/artist/:id" component={ SingleArtist } />
              </Switch>
            </ContentWrapper>
          <FooterComponent />
        </Fragment>
      </Router>
    )
  }
}

const ArtistsClasses = {
  main: "ItemsListing",
  wrapper: "ItemsListing__wrapper"
}

export default App
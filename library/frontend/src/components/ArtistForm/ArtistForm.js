import React, { Component, Fragment } from 'react'
import PropTypes from 'prop-types'
import { Transition } from 'react-transition-group'
import axios from 'axios'

 class ArtistForm extends Component {

  state = {
    update: false,
    artistFormError: false,
    formFields: {
      name: '',
      country: '',
      genre: '',
      image: '',
      body: ''
    },
    InvalidFields: {
      name: false,
      country: false,
      genre: false
    }
  }

  onUpdateFormFieldsHandler = e => {
    const fieldName = e.target.name
    const fieldValue = e.target.value

    this.setState( prevState => ({
      formFields: {
        ...prevState.formFields,
        [fieldName]: fieldValue
      }
    }))
    this.validateField(fieldName, fieldValue)
  }

  validateField(fieldName, fieldValue) {
    this.setState( prevState => ({
      InvalidFields: {
        ...prevState.InvalidFields,
        [fieldName]: fieldValue.length < 1
      }
    }))
  }

  proceedForm = e => {
    e.preventDefault()
    const newEditArtist = {
      name: this.state.formFields.name,
      genre: this.state.formFields.genre,
      country: this.state.formFields.country,
      image: this.state.formFields.image,
      body: this.state.formFields.body,
    }

    for(let key in newEditArtist ) {
      if (newEditArtist.hasOwnProperty(key)) {
        this.validateField(key, newEditArtist[key])
     }
    }

    if(this.state.update) {
      axios.put('http://localhost:8000/api/artists/' + this.props.match.params.id + '/', newEditArtist)
      .then( response => {
        this.props.history.replace('/artist/' + this.props.match.params.id)
      })
      .catch( error => {
        this.setState({
          artistFormError: true
        })
      })  
    }
    else {
      axios.post('http://localhost:8000/api/artists/', newEditArtist)
      .then( response => {
        this.props.history.replace('/')
      })
      .catch( error => {
        this.setState({
          artistFormError: true
        })
      })
    }
  }

  cancelFormHandler = e => {
    this.props.history.goBack()
  }

  removeArtistError = e => {
    setTimeout(() => {
      this.setState({
        artistFormError: false
      })
    }, 4000);
  }

  componentDidMount() {
    if( this.props.match.params.id ) {
      axios.get('http://localhost:8000/api/artists/' + this.props.match.params.id)
      .then( response => {
        this.setState({
          update: true,
          formFields: response.data
        })
      })
    }
  }

  componentDidUpdate(prevProps) {
    if( this.props.match.params.id !== prevProps.match.params.id) {
      this.setState({
        update: false,
        formFields: {
          name: '',
          country: '',
          genre: '',
          image: '',
          body: ''
        }
      })
    }
  }

  render() {
    return (
      <Fragment>
        <form className="ArtistForm">
          <div className="ArtistForm__container container">
            <div className="ArtistForm__header">
              <div className="ArtistForm__column">
                <div className="ArtistForm__inputWrapper Form__row">
                  <label htmlFor="name" className="ArtistForm__label Form__label">Artist name*</label>
                  <input type="text" name="name" id="name" value={ this.state.formFields.name } onChange={ this.onUpdateFormFieldsHandler } className={"ArtistForm__input Form__input" + ( this.state.InvalidFields.name ? ' is-invalid' : '' ) }/>
                </div>
                <div className="ArtistForm__inputWrapper Form__row">
                  <label htmlFor="country" className="ArtistForm__label Form__label">Artist Country*</label>
                  <input type="text" name="country" id="country" value={ this.state.formFields.country } onChange={ this.onUpdateFormFieldsHandler } className={"ArtistForm__input Form__input" + ( this.state.InvalidFields.country ? ' is-invalid' : '' ) } />
                </div>
                <div className="ArtistForm__inputWrapper Form__row">
                  <label htmlFor="genre" className="ArtistForm__label Form__label">Artist Genre*</label>
                  <input type="text" name="genre" id="genre" value={ this.state.formFields.genre } onChange={ this.onUpdateFormFieldsHandler } className={"ArtistForm__input Form__input" + ( this.state.InvalidFields.genre ? ' is-invalid' : '' ) }/>
                </div>
                <div className="ArtistForm__inputWrapper Form__row">
                  <label htmlFor="image" className="ArtistForm__label Form__label">Artist image (URL)</label>
                  <input type="text" name="image" id="image" value={ this.state.formFields.image } onChange={ this.onUpdateFormFieldsHandler } className="ArtistForm__input Form__input" />
                </div>
              </div>
              <figure className="ArtistForm__figure">
                { this.state.formFields.image ? (
                  <img src={ this.state.formFields.image } className="ArtistForm__image"/>
                ) : ( 
                <p className="t-paragraph t-paragraph--x-large">Image Preview</p> 
                )}
              </figure>
            </div>
            <div className="ArtistForm__textareaWrapper Form__row">
              <label htmlFor="body" className="ArtistForm__label Form__label">Artist Body</label>
              <textarea type="text" name="body" id="body" value={ this.state.formFields.body } onChange={ this.onUpdateFormFieldsHandler } className="ArtistForm__textarea Form__input"></textarea>
            </div>
            <button className="ArtistForm__btn ArtistForm__btn--submit btn" onClick={ this.proceedForm }>Submit</button>
            <button type="button" className="ArtistForm__btn btn btn--secondary" onClick={ this.cancelFormHandler }>Cancel</button>
            <Transition in={ this.state.artistFormError } timeout={ 800 } mountOnEnter unmountOnExit onEntered={ this.removeArtistError }>
              { state => (
                <p className={ "Form__error t-paragraph " + (state === 'entering' ? ' is-entering' : state === 'exiting' ? ' is-exiting' : '') } >Something went wrong. Please check if all required fields are completed.</p>
              )}
            </Transition>
          </div>
        </form>
      </Fragment>
    )
  }
}

ArtistForm.propTypes = {
  match: PropTypes.shape({
    params: PropTypes.shape({
      id: PropTypes.string
    })
  }),
  history: PropTypes.shape({
    replace: PropTypes.func
  })
}

export default ArtistForm
import React from 'react'

const FooterComponent = () => (
    <footer className="Footer">
      <div className="Footer__container container">
        <p className="t-paragraph t-paragraph--white">This is simple footer text</p>
      </div>
    </footer>
  )


export default FooterComponent

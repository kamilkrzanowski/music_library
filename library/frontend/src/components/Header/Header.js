import React from 'react'
import { NavLink, Link, withRouter } from 'react-router-dom'
import { Transition } from 'react-transition-group'
import PropTypes from 'prop-types'

const HeaderComponent = props => {

  const closeDropdownHandler = e => {
    document.querySelector('[data-filter-input]').value = ''
    document.querySelector('[data-filter-result]').classList.remove('is-active')
  }

  const moveBackHandler = e => {
    props.history.goBack()
  }

  return (
    <nav className="HeaderComponent">
      <div className="HeaderComponent__container container">
        { props.history.location.pathname !== '/' ? (
          <button className="HeaderComponent__arrow" onClick={ moveBackHandler }>
            <span className="HeaderComponent__arrowPart"></span>
          </button>
        ) : null }
        <div className="HeaderComponent__menu" data-menu>
          <NavLink to="/" exact className="HeaderComponent__link" onClick={ props.toggleMenu }>Home</NavLink>
          <NavLink to="/add-artist" className="HeaderComponent__link" onClick={ props.toggleMenu }>Add new Artist</NavLink>
        </div>
        <Transition in={ props.menuState } timeout={ 200 } mountOnEnter unmountOnExit>
          { state => (
            <div className={ "HeaderComponent__menuDropdown" + ( state === 'entering' || state === 'entered' ? ' is-active' : '')} onClick={ props.toggleMenu }></div>
          )}
        </Transition>
        <div className="HeaderComponent__filter">
          <input className="HeaderComponent__filterInput Form__input" type="text" placeholder="Search in artists" onChange={ props.changed } onBlur={ closeDropdownHandler } data-filter-input />
          <ul className="HeaderComponent__filterResults" data-filter-result>
            { props.results.map( result => (
              <li key={ result.id } className="HeaderComponent__filterResultsItem">
              <Link to={ "/artist/" + result.id } onClick={ closeDropdownHandler }>{ result.name }</Link>
              </li>
            ))}
          </ul>
        </div>
        <Transition in={ props.menuState } timeout={ 200 }>
          { state => (
            <button className={ "HeaderComponent__hamburger" + ( state === 'entering' || state === 'entered' ? ' is-active' : '') } onClick={ props.toggleMenu }>
              <span className="HeaderComponent__hamburgerPart"></span>
            </button>
          )}
        </Transition>
      </div>
    </nav>
  )
}

HeaderComponent.propTypes = {
  results: PropTypes.array.isRequired,
  changed: PropTypes.func.isRequired,
  toggleMenu: PropTypes.func.isRequired,
  menuState: PropTypes.bool
}

export default withRouter(HeaderComponent)
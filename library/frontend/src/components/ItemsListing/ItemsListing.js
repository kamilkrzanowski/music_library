import React from 'react'
import PropTypes from 'prop-types'

import ListItem from '../ListItem/ListItem'

const ItemsListing = props => {
  const { 
    classes, 
    items,
  } = props

  return (
    <section className={ classes.main }>
      <div className="container">
        <div className={ classes.wrapper }>
          { items.map( (item) => 
            <ListItem key={ item.id } item={ item }></ListItem>
          ) }
        </div>
      </div>
    </section>
  )
}
  
ItemsListing.propTypes = {
  items: PropTypes.array.isRequired,
  classes: PropTypes.object,
}

export default ItemsListing
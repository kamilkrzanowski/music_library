import React from 'react'
import { Link } from 'react-router-dom'
import PropTypes from 'prop-types'

const ListItem = ( props ) => {
  const { 
    id,
    name, 
    image, 
    country,
    genre, 
    body,
  } = props.item

  const BackgroundStyles = { 
    backgroundImage: 'url(' + image + ')',
  }

  const ReadMore = body.length > 100 ? '...' : ''

return (
  <article className="ListItem" style={ BackgroundStyles }>
    <Link to={ "artist/" + id }>
      <div className="ListItem__content">
        <h3 className="ListItem__heading t-heading t-heading--white"> { name }</h3>
        <div className="ListItem__meta">
          <p className="ListItem__country t-paragraph t-paragraph--small t-paragraph--white"> { country }</p>
          <p className="ListItem__genre t-paragraph t-paragraph--small t-paragraph--white"> { genre }</p>
        </div>
        <p className="ListItem__copy t-paragraph t-paragraph--white"> { body.substring(0, 100) + ReadMore }</p>
      </div>
    </Link>
  </article>
  )
}

ListItem.propTypes = {
  item: PropTypes.object.isRequired,
}

export default ListItem
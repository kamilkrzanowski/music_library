import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'
import axios from 'axios'

import AlbumList from '../AlbumList/AlbumList'
import Modal from '../_ui/Modal/Modal'

class SingleArtist extends Component {

  constructor(props){
    super(props);
    this.state = {
      artist: {},
      showDeleteArtistModal: false,
      deleteModal: React.createRef()
    }
  }

  getSingleArtis = () => {
    axios.get('http://localhost:8000/api/artists/' + this.props.match.params.id )
    .then( response => {
      this.setState({ artist: response.data })
    })
  }

  onDeleteArtist = e => {
    e.preventDefault()
    axios.delete('http://localhost:8000/api/artists/' + this.props.match.params.id )
      .then( response => {
        this.props.history.replace('/')
      })
  }

  openModalHandler = e => {
    this.setState({
      showDeleteArtistModal: true
    })
    window.scrollTo(0, this.deleteModal)
  }

  closeModalHandler = e => {
    this.setState({
      showDeleteArtistModal: false
    })
  }

  componentDidMount = () => {
    this.getSingleArtis()
  }

 componentDidUpdate = prevProps => {
    if(this.props.match.params.id !== prevProps.match.params.id) {
      this.getSingleArtis()
    }
 }

  render() {

    const { id } = this.props.match.params
    
    return (
      <section className="SingleArtist">
        <div className="SingleArtist__container container">
          <header className="SingleArtist__header">
            <div className="SingleArtist__info">
              <h2 className="SingleArtist__heading t-heading">{ this.state.artist.name }</h2>
              <p className="SingleArtist__body t-paragraph">{ this.state.artist.body }</p>
              <div className="SingleArtist__meta">
                <p className="SingleArtist__genre t-paragraph t-paragraph--white">{ this.state.artist.genre }</p>
                <p className="SingleArtist__country t-paragraph t-paragraph--white">{ this.state.artist.country }</p>
              </div>
              <div className="SingleArtist__bottom">
                <p className="SingleArtist__date t-paragraph">
                { this.state.artist.created_at ? (
                  'Created at: ' +
                   new Intl.DateTimeFormat('en-GB', {
                   year: 'numeric', 
                   month: 'long', 
                   day: '2-digit' 
                  }).format( new Date(this.state.artist.created_at.toString()) )
                ) : null }
                </p>
                <div className="SingleArtist__buttons">
                  <Link className="SingleArtist__btn t-paragraph t-paragraph--small t-paragraph--upper t-paragraph--bold" to={ "/edit-artist/" + id }>Edit</Link>
                  <button className="SingleArtist__btn SingleArtist__btn--delete t-paragraph t-paragraph--small t-paragraph--upper t-paragraph--bold" onClick={ this.openModalHandler }>Delete</button>
                </div>
              </div>
            </div>
            <figure className="SingleArtist__figure">
              <img src={ this.state.artist.image } className="SingleArtist__image" />
            </figure>
          </header>
          <AlbumList artist_id={ id }/>
        </div>
        <Modal 
          ref={ this.deleteModal }
          title="Delete artist?" 
          inProp={ this.state.showDeleteArtistModal } 
          submitTrigger={ this.onDeleteArtist } 
          closeTrigger={ this.closeModalHandler }
          cancel
         />
      </section>
    )
  }
}

SingleArtist.propTypes = {
  match: PropTypes.shape({
    params: PropTypes.shape({
      id: PropTypes.string.isRequired
    })
  })
}

export default SingleArtist
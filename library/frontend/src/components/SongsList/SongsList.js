import React, { Component } from 'react'
import axios from 'axios'
import { TransitionGroup, Transition } from 'react-transition-group'
import PropTypes from 'prop-types'

class SongsList extends Component {
  constructor(props) {
    super(props)
    this.state = {
      songs: [],
      newSongName: '',
      newSongEror: false,
      deleteSongPrompt: false
    }
  }

  onNewSongChangeHandler = e => {
    const fieldValue = e.target.value
    
    this.setState({
      newSongName: fieldValue
    })
  }

  addNewSongHandler = e => {
    e.preventDefault()

    const newSong = {
      album_id: this.props.album_id,
      name: this.state.newSongName
    }

    axios.post('http://localhost:8000/api/songs/', newSong )
    .then( response => {
      this.setState( prevState => ({
        songs: [
          ...prevState.songs, 
          response.data
        ],
        newSongName: ''
      }))
    })
    .catch( error => {
      this.setState({
        newSongEror: true
      })
    })
  }

  removeError = e => {
    setTimeout(() => {
      this.setState({
        newSongEror: false
      })
    }, 4000);
  }

  toggleDeleteConfirmation = e => {
    this.setState( prevState => ({
      deleteSongPrompt: !prevState.deleteSongPrompt
    }))
  }

  deleteSongHandler = id => {
    axios.delete('http://localhost:8000/api/songs/' + id + '/')
    .then(response => {
      this.setState( prevState => ({
        songs: [
          ...prevState.songs.filter( song => 
          song.id !== id )
        ]
      }))
    })
  }

  getAlbumSongs = () => {
    axios.get('http://localhost:8000/api/songs?album_id=' + this.props.album_id )
      .then( response => {
        this.setState({
          songs: response.data
        })
      })
  }

  componentDidMount() {
    this.getAlbumSongs()
  }

  render() {
    
    const {
      toggleModal,
      album_id
    } = this.props
  
    return (
      <div className="SongsList">
        <div className="SongsList__grid">
          <TransitionGroup className="SongsList__list" component="ul">
            { this.state.songs.map( song => (
              <Transition key={ song.id } timeout={ 400 }>
              { state => (
                <li className={"SongsList__item" + (state === 'entering' ? ' is-entering' : state === 'exiting' ? ' is-exiting' : '') }>
                  <p className="SongsList__itemName t-paragraph">{ song.name }</p> 
                    { this.state.deleteSongPrompt ? (
                    <div className="SongsList__deletePrompt">
                      <button className="SongsList__itemBtn t-paragraph t-paragraph--small t-paragraph--upper t-paragraph--bold" onClick={ this.deleteSongHandler.bind(this, song.id) }>Confirm</button>
                      <button className="SongsList__itemBtn SongsList__itemBtn--cancel t-paragraph t-paragraph--small t-paragraph--upper t-paragraph--bold" onClick={ this.toggleDeleteConfirmation }>Cancel</button>
                    </div>
                    ) : null }
                    { !this.state.deleteSongPrompt ? (
                      <button className="SongsList__itemBtn t-paragraph t-paragraph--small t-paragraph--upper t-paragraph--bold" onClick={ this.toggleDeleteConfirmation }> Delete </button> 
                    ) : null }
                </li>
              )}
              </Transition>
              ))}
          </TransitionGroup>
          <form className="SongsList__newSong">
            <div className={ "SongsList__newSongWrapper" + (this.state.newSongEror ? ' is-invalid' : '') }>
              <label className="SongsLists__newSongLabel">
                <p className="SongLists__LabelHidden">New song's name*</p>
                <input type="text" name="new_song" className="SongsLists__newSongInput" placeholder="New song's name..." value={ this.state.newSongName } onChange={ this.onNewSongChangeHandler }/>
              </label>
              <button className="SongsList__newSongBtn t-paragraph t-paragraph--small t-paragraph--upper t-paragraph--bold" onClick={ this.addNewSongHandler }>Add new</button>
            </div>
            <Transition in={ this.state.newSongEror } timeout={ 800 } mountOnEnter unmountOnExit onEntered={ this.removeError }>
              { state => (
                <p className={ "SongsList__errorText t-paragraph" + (state === 'entering' ? ' is-entering' : state === 'exiting' ? ' is-exiting' : '') }> Oops... Something went wrong</p>
              )}
            </Transition>
          </form>
          <div className="SongsList__side">
            <button className="SongsList__sideBtn SongsList__headerBtn--with-margin btn btn--outline btn--xsmall" onClick={ toggleModal.bind(this, 1, album_id) }>Edit Album</button>
            <button className="SongsList__sideBtn btn btn--outline btn--xsmall" onClick={ toggleModal.bind(this, 2, album_id) }>Delete Album</button>
          </div>
        </div>
      </div>
    )
  }
}

SongsList.propTypes = {
  toggleModal: PropTypes.func.isRequired,
  album_id: PropTypes.number.isRequired
}

export default SongsList

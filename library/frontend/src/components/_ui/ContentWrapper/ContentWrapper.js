import React from 'react'
import PropTypes from 'prop-types'

const ContentWrapper = props => (
  <div className="ContentWrapper">{ props.children }</div>
)

ContentWrapper.propTypes = {
  children: PropTypes.node
}

export default ContentWrapper

import React from 'react'
import PropTypes from 'prop-types'
import { Transition } from 'react-transition-group';

const Modal = (props) =>  {

  const {
    submitTrigger,
    closeTrigger,
    title,
    cancel,
    children,
    inProp
  } = props

  return (
  <Transition in={ inProp } timeout={ 300 } mountOnEnter unmountOnExit>
  { state => (
  <div className={ "Modal " + state }>
    <form className="Modal__form">
      <div className="Modal__formInner">
        <span className="Modal__closeBtn t-heading t-heading--large t-heading--white" onClick={ closeTrigger }>&times;</span> 
        { title ? (
          <p className="Modal__heading t-paragraph t-paragraph--large t-paragraph--bold t-paragraph--center">{ title }</p>
        ) : null }
        { children }
        <div className="Modal__buttons">
          <button className="Modal__btn Modal__btn--submit btn" onClick={ submitTrigger }>Submit</button>
        { cancel ? (
          <button type="button" className="Modal__btn btn btn--secondary" onClick={ closeTrigger }>Cancel</button>
        ) : null } 
        </div>
      </div>
    </form> 
  </div> 
    )}
  </Transition>
  )
}

Modal.propTypes = {
  inProp: PropTypes.bool.isRequired,
  submitTrigger: PropTypes.func.isRequired,
  closeTrigger: PropTypes.func.isRequired,
  title: PropTypes.string,
  cancel: PropTypes.bool,
  children: PropTypes.node
}

export default Modal

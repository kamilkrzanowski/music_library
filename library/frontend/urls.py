from django.urls import path 
from . import views 

urlpatterns = [
  path('', views.index),
  path('artist', views.index),
  path('artist/<int:id>', views.index),
  path('add-artist', views.index),
  path('edit-artist/<int:id>', views.index),
  path('album', views.index),
  path('album/<int:id>', views.index),
]

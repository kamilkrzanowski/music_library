from .models import Song 
from rest_framework import viewsets, permissions
from .serializers import SongSerializer 

# Artists Viewset

class SongsViewset(viewsets.ModelViewSet):
  permission_classes = [
    permissions.AllowAny
  ]
  serializer_class = SongSerializer
  
  def get_queryset(self):
    album_id = self.request.query_params.get('album_id')

    if(album_id):
      queryset = Song.objects.filter(album_id = album_id)
    else:
      queryset = Song.objects.all()
    return queryset
# Generated by Django 2.1.7 on 2019-03-14 10:30

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('songs', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='song',
            name='album_id',
            field=models.IntegerField(default=0),
        ),
        migrations.AddField(
            model_name='song',
            name='name',
            field=models.CharField(default='a', max_length=200),
        ),
    ]

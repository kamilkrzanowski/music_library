from django.db import models

# Create your models here.

class Song(models.Model):
  album_id = models.IntegerField(default = 0)
  name = models.CharField(max_length = 200)

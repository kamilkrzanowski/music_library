from rest_framework import serializers 
from .models import Song

# Artists Serializer 

class SongSerializer(serializers.ModelSerializer): 
  class Meta:
    model = Song
    fields = '__all__'

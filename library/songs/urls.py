from rest_framework import routers
from .api import SongsViewset

router = routers.DefaultRouter()
router.register('api/songs', SongsViewset, 'songs')
router.register('api/songs/?album_id=<int:id>', SongsViewset, base_name='songs')
urlpatterns = router.urls
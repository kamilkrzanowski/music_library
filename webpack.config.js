const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const path = require('path')
const devMode = process.env.NODE_ENV !== 'production'

module.exports = {
  mode: devMode ? 'development' : 'production',
  entry: {
    main: './library/frontend/src/index.js', 
    style : './library/frontend/src/scss/app.scss'
  },
	output: {
		path: path.resolve(__dirname, './library/frontend/static/frontend'),
    filename: '[name].js'
	},
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: "babel-loader"
        }
      },
      {
        test: /\.scss$/,
        use: [
          {
            loader: MiniCssExtractPlugin.loader,
            options: {
              // you can specify a publicPath here
              publicPath: '../'
            }
          },
          {
            loader: 'css-loader', 
            options: {
              sourceMap: true,
            }
          },
          {
            loader: "postcss-loader",
            options: {
              ident: 'postcss',
              plugins: [
                require('autoprefixer')({ grid: "autoplace" }),
                require('cssnano')({ preset: 'default' })
              ]
            }
          },
          {
              loader: "sass-loader",
              options: {
                sourceMap: true
              }
          }
          
        ]
      },
      { 
        test: /\.(jpg|png|woff|woff2|eot|ttf|svg)$/, 
        loader: 'url-loader' 
      }
    ]
  },
  plugins: [
    new MiniCssExtractPlugin({
      filename: 'styles.css',
    }),
  ]
};